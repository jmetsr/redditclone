class SessionsController < ApplicationController

  def new
    @user = User.new
    render :new
  end

  def create
    username = session_params[:username]
    password = session_params[:password]
    @user = User.new(session_params)
    @current_user = find_by_credentials(username, password)
    if @current_user
      self.login(@current_user)
      redirect_to user_url(current_user)
    else
      flash.now[:errors] = "invalid username or password"
      render :new
    end
  end

  def destroy
    self.logout
    redirect_to new_session_url
  end

  private
  def session_params
    params.require(:user).permit(:username, :password)
  end

end