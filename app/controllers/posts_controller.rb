class PostsController < ApplicationController

  def new
    @post = Post.new
    render :new
  end

  def create
    author_params = {:author_id => current_user.id}
    new_params = post_params.merge({:sub_id => params[:sub_id]})
    new_params = new_params.merge(author_params)
    @post = Post.new(new_params)
    if @post.save
      redirect_to sub_url(@post.sub)
    else
      flash.now[:errors] = @post.errors.full_messages
      render :new
    end
  end

  def destroy
    @post = Post.find(params[id]).destroy
    redirect_to sub_url(@post.sub)
  end

  def edit
    @post = Post.find(params[id])
    render :edit
  end

  def show
    @post = Post.find(params[id])
    render :show
  end

  private
  def post_params
    params.require(:post).permit(:url, :title, :content)
  end
end