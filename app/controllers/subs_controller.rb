class SubsController < ApplicationController
  def index
    render :index
  end
  def show
    @sub = Sub.find(params[:id])
    render :show
  end
  def create
    new_params = {"moderator_id" => current_user.id}.merge(sub_params)
    @sub = Sub.new(new_params)
    if @sub.save
      redirect_to sub_url(@sub)
    else
      flash.now[:errors] = @sub.errors.full_messages
      render :new
    end
  end
  def destroy
    @sub = Sub.find(params[:id])
    @sub.destroy
    render :index
  end
  def edit
    @sub = Sub.find(params[:id])
    render :edit
  end
  def update
    new_params = {"moderator_id" => current_user.id}.merge(sub_params)
    @sub = Sub.new(new_params)
    if @sub.save
      redirect_to sub_url(@sub)
    else
      flash.now[:errors] = @sub.errors.full_messages
      render :new
    end
  end
  def new
    @sub = Sub.new
    render :new
  end

  private
  def sub_params
    params.require(:sub).permit(:title, :description)
  end
end