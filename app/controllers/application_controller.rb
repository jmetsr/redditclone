class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def login(user)
    session[:session_token] = user.session_token
  end

  def logout
    current_user.reset_session_token
    session[:session_token] = nil
  end

  def current_user
    User.find_by_session_token(session[:session_token])
  end

  def logged_in?
    return current_user.nil? ? false : true
  end

  def find_by_credentials(username, password)
    user = User.find_by_username(username)
    if user
      if user.password_is?(password)
        return user
      end
    end

    nil
  end
end
