require 'bcrypt'
class User < ActiveRecord::Base
  include BCrypt
  validates :username, :session_token, :password_digest, presence: true
  validates :username, :session_token, uniqueness: true
  validates :password, length: { minimum: 6, allow_nil: true}

  after_initialize :set_session_token

  has_many(
    :subs,
    :class_name => "Sub",
    :foreign_key => :moderator_id,
    :primary_key => :id,
    :inverse_of => :moderator
  )
  has_many(
  :posts,
  :class_name => "Post",
  :foreign_key => :author_id,
  :primary_key => :id,
  :inverse_of => :author
  )
  def password=(password)
    @password = password
    self.password_digest = BCrypt::Password.create(password)
  end

  def password
    @password
  end

  def password_is?(password)
    BCrypt::Password.new(self.password_digest) == password
  end

  def set_session_token
    self.session_token ||= SecureRandom.urlsafe_base64
  end

  def reset_session_token
    self.session_token = SecureRandom.urlsafe_base64
    self.save!
    self.session_token
  end
end