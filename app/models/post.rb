class Post < ActiveRecord::Base
  validates :title, :sub_id, :author_id, presence: true
  belongs_to(
    :sub,
    :inverse_of => :posts
  )

  belongs_to(
    :author,
    :class_name => "User",
    :foreign_key => :author_id,
    :primary_key => :id,
    :inverse_of => :posts
  )

end
