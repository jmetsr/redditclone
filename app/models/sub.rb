class Sub < ActiveRecord::Base
  validates :title, :moderator_id, :description, presence: true
  has_many(
    :posts,
    :class_name => "Post",
    foreign_key: :sub_id,
    primary_key: :id,
    inverse_of: :sub
  )
  belongs_to(
    :moderator,
    :class_name => "User",
    foreign_key: :moderator_id,
    primary_key: :id,
    inverse_of: :subs
  )
end