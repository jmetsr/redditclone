RedditClone::Application.routes.draw do
  resources :users
  resource :session, only: [:new, :create, :destroy]
  resources :subs do
    resources :posts, only: [:new, :create, :destroy, :edit, :show]
  end

end
